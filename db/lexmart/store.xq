xquery version "3.0";

import module namespace templates = "http://exist-db.org/xquery/templates";
import module namespace config="http://per-fide.di.uminho.pt/academia/config" at "modules/config.xqm";
import module namespace app="http://per-fide.di.uminho.pt/academia/templates" at "modules/app.xql";
import module namespace academia="http://per-fide.di.uminho.pt/academia" at "modules/functions.xql" ;
import module namespace functx    = "http://www.functx.com" at "/db/system/repo/functx-1.0/functx/functx.xql";
import module namespace util = "http://exist-db.org/xquery/util";


declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare namespace json="http://www.json.org";

(: Switch to JSON serialization :)
declare option output:method "json";
declare option output:media-type "text/javascript";

let $lookup := function($functionName as xs:string, $arity as xs:int) {
    try {
        function-lookup(xs:QName($functionName), $arity)
    } catch * {
        ()
    }
}

let $config := map {
    $templates:CONFIG_APP_ROOT := $config:app-root,
    $templates:CONFIG_STOP_ON_ERROR := true(),
    $templates:CONFIG_FN_RESOLVER := $lookup,
    $templates:CONFIG_PARAM_RESOLVER := $lookup
}
let $all := map {
    $templates:CONFIGURATION := $config
}

let $entry := request:get-parameter("uri", ())
let $formatted := xmldb:decode-uri($entry)
let $collection := util:collection-name($formatted)
let $name := xmldb:decode(util:document-name($formatted))
let $xml := request:get-data()
let $ok := xmldb:store($collection, $name, $xml)
return if ($ok = ())
   then <status json:literal="false">false</status>
   else <status json:literal="true">true</status>