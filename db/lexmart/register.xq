xquery version "1.0";

declare option exist:serialize "method=html5 media-type=text/html";
declare variable $searchphrase := request:get-parameter("register", ());

<html>
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <title>Dicionário da Academia das Ciências de Lisboa</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous"/><!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous"/><!-- My Own styles -->
        <link rel="stylesheet" type="text/css" href="resources/css/style.css"/>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
    </head>
    <body>
     <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header"><button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="navbar-brand" href="index.html">Dicionário da Academia das Ciências de Lisboa</a>
                </div>
            </div>
        </nav>
     <div style="margin-top: 60px" class="container">
                 <div id="content">
        <div style="float: right"><a href="index.html">Voltar</a></div>
        <h4>Registo: { $searchphrase }</h4>
                <div id="results">
        {
let $usgs := collection("/db/academia")//usg[@type="register" and starts-with(text(), $searchphrase)] 
for $usg in $usgs
let $parent := $usg/..
let $terms := $usg/ancestor::re/form/orth | $usg/ancestor::entry/form/orth | $usg/ancestor::sense/form/orth
let $term := $terms[1]
order by $term
return <div>
     <h4>{$term}</h4>
     <div class="entry"> { $parent } </div>
     </div>
}
</div></div></div>
        <script type="text/javascript" src="resources/js/main.js"></script>
</body>
</html>