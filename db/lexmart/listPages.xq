xquery version "3.0";

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "json";
declare option output:media-type "application/json";

<pages>
{
for $a in collection("/db/academiaPages")
let $title := $a//h1[1]/text()
let $uri := fn:base-uri($a)
return <page><uri>{$uri} </uri> <title> {$title} </title></page>
}
</pages>

(:

#pagesMenu

outputs something like

{

    "page": [
        {
            "uri": "/db/academiaPages/teste.xml",
            "title": "teste"
        },
        {
            "uri": "/db/academiaPages/teste2.xml",
            "title": "teste2"
        }
    ]

}

:)