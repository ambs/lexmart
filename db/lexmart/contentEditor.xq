xquery version "3.0";

import module namespace templates = "http://exist-db.org/xquery/templates";
import module namespace config    = "http://per-fide.di.uminho.pt/academia/config"    at "modules/config.xqm"    ;
import module namespace app       = "http://per-fide.di.uminho.pt/academia/templates" at "modules/app.xql"       ;
import module namespace academia  = "http://per-fide.di.uminho.pt/academia"           at "modules/functions.xql" ;
import module namespace functx    = "http://www.functx.com"    at "/db/system/repo/functx-1.0/functx/functx.xql" ;

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";

declare option output:method     "html5";
declare option output:media-type "text/html";

let $lookup := function($functionName as xs:string, $arity as xs:int) {
    try {
        function-lookup(xs:QName($functionName), $arity)
    } catch * {
        ()
    }
}

let $config := map {
    $templates:CONFIG_APP_ROOT       := $config:app-root,
    $templates:CONFIG_STOP_ON_ERROR  := true(),
    $templates:CONFIG_FN_RESOLVER    := $lookup,
    $templates:CONFIG_PARAM_RESOLVER := $lookup
}
let $all := map {
    $templates:CONFIGURATION := $config
}

let $uri  := request:get-parameter("page", ())

let $text := <div>
  <input type="hidden" value="{$uri}" id="uri"/>
  <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
  <script><![CDATA[
  
    var existUri;
    function load_page(inst, uri) {
        existUri = uri;
        $.ajax({
            url: 'http://per-fide.di.uminho.pt:8080/exist/rest' + uri,
            dataType: "text", cache: false}
        )
        .done(
            function(xml) {
               inst.setContent(xml);
        }
    );
}
  
    $(function() {
    tinymce.init({ selector:'#editor',
                   plugins: "table",
                     min_height: 400,
                   menubar: 'edit insert view format table tools',
                       init_instance_callback: "insert_contents",
                  toolbar: 'saveButton | undo redo  | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
               
                  setup: function (editor) {
                    editor.addButton('saveButton', {
                        title: 'Guardar',
                        image: 'resources/img/ico_guardar.png',
                        onclick: function () {
                            var xml = htmldecode(tinymce.activeEditor.getContent({format: 'xml'}));
                            if (!xml.match(/^<div/)) {
                                xml = "<div>" + xml + "</div>";
                            }
                          //  console.log(xml);
                            var url = "http://per-fide.di.uminho.pt:8080/exist/apps/academia/store.xq?uri=" + encodeURIComponent(existUri);
                            $.ajax({ type: "POST", url: url, data: xml, cache: false, contentType: "text/xml", processData: false })
                             .success(function(result) { if (result) { alert("Guardado!"); } else { alert("Erro ao guardar."); } });
                        }
                    });      
            }});
    });
    
    function insert_contents(inst){
        load_page(inst, $("#uri").val());  
    }
  ]]></script>
  <textarea id="editor"></textarea>
</div>

return templates:surround($text, $all, "templates/page.html", "content", "","")
