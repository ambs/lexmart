xquery version "3.0";
module namespace academia="http://per-fide.di.uminho.pt/academia";

import module namespace templates = "http://exist-db.org/xquery/templates" ;
import module namespace functx    = "http://www.functx.com" at "/db/system/repo/functx-1.0/functx/functx.xql";


declare
  %templates:wrap
  function academia:new-page($node as node()*, $model as map(*)) as element(div) {
    if (academia:can-edit()) then
        let $term := functx:trim(request:get-parameter("page","a"))
        return if (academia:page-available($term)) then
            let $resource := academia:createPage($term, concat($term, ".xml"))
            return <div>
                 <h2>Página criada!</h2>
                   Editar: <a href="oxygen:/academia$academia{ $resource }">{ xmldb:decode-uri( $resource ) } </a>
                 </div>
        else
            <div><h2>Página indisponível</h2>
                <p>Verifique se a página que pretende adicionar já existe.</p></div>
    else
        <h2>Não tem permissões para adicionar páginas</h2>
};

declare
  %templates:wrap
  function academia:new-term($node as node()*, $model as map(*)) as element(div) {
    if (academia:can-edit()) then
        let $term := functx:trim(request:get-parameter("term","a"))
        return if (academia:term-available($term)) then
            let $resource := academia:createDoc($term, concat($term, ".xml"))
            return <div>
                 <h2>Verbete criado!</h2>
                   Editar: <a href="oxygen:/academia$academia{ $resource }">{ xmldb:decode-uri( $resource ) } </a>
                 </div>
        else
            <div><h2>Termo indisponível</h2>
                <p>Verifique se o termo que pretende adicionar já existe, com ou sem desdobramento. Para criar um
                    novo desdobramento use <tt>termo:4</tt> com o valor correto.
                </p></div>
    else
        <h2>Não tem permissões para adicionar verbetes</h2>
};

declare function academia:valid-user() as xs:boolean {
    let $username := sm:id()//sm:username/text()
    return if ($username = "admin" or $username = "ana" or $username = "ruiclaro") then true() else false()
};

declare function academia:can-edit() as xs:boolean {
    let $username := sm:id()//sm:username/text()
    let $groups := sm:get-user-groups($username)
    let $isEditor := contains($groups, "editor")
    return if (academia:valid-user() or $isEditor ) then true() else false()
};

declare
  %templates:wrap
  function academia:filters($node as node()*, $model as map(*)) as element(div) {
      if (academia:valid-user()) then
        <div class="row">
        <div class="col-sm-12">
            <form method="POST" id="genForm" class="form-inline">
            Filtros:
            <div class="form-group">
                    <input type="radio" id="r1" name="searchType" value="geo"/>
                    <label for="r1"> Marcas geográficas</label>
                    <input type="radio" id="r2" name="searchType" value="dom"/>
                    <label for="r2"> Marcas de especialidade</label>
                    <input type="radio" id="r3" name="searchType" value="reg"/>
                    <label for="r3"> Marcas de registo</label>
                    <input type="radio" id="r4" name="searchType" value="gram"/>
                    <label for="r4"> Marcas gramaticais</label>
                    <input type="radio" id="r5" name="searchType" value="etym"/>
                    <label for="r5"> Étimo</label>
                    <input type="radio" id="r6" name="searchType" value="hint"/>
                    <label for="r6"> Outros</label>
                    <select style="margin-right: 30px; margin-left: 30px" id="generic" name="generic" class="form-control">
                        <option>----------------------</option>
                    </select>
                    <input type="submit" value="Procurar" class="btn btn-default btn-sm"></input>
                </div>
            </form>
        </div></div>
    else <div></div>
};
      
declare
  %templates:wrap
  function academia:tools($node as node()*, $model as map(*)) as element(div) {
      
  if (academia:valid-user()) then
     <div class="row">
        <div class="col-sm-6">
            <h4>Estatísticas</h4>
            <form action="stats.html">
                <button type="submit" class="btn btn-default">Estatísticas por entrada/classe gramatical</button>
            </form>
            <form action="sstats.html">
                <button type="submit" class="btn btn-default">Estatísticas por aceção</button>
            </form>
        </div>
        <div class="col-sm-6">
            <h4>Relatórios</h4>
            <form action="global-report.xql">
                <input type="hidden" name="type" value="new"/>
                <button type="submit" class="btn btn-default">Entradas novas</button>
            </form>
            <form action="global-report.xql">
                <input type="hidden" name="type" value="edited"/>
                <button type="submit" class="btn btn-default">Entradas editadas</button>
            </form>
            <form action="global-report.xql">
                <input type="hidden" name="type" value="revised"/>
                <button type="submit" class="btn btn-default">Entradas revistas</button>
            </form>
        </div>
    </div>
    else <div></div>
  };

declare
  %templates:wrap
  function academia:new-term-box($node as node()*, $model as map(*)) as element(div) {
    if (academia:can-edit()) then
        <div class="alert alert-danger" role="alert" style="margin-top: 15px">
        <form action="new.html" method="POST">
            <div class="form-group">
                <label for="new" style="margin-right: 10px;">Nova entrada:</label>
                <input type="text" size="40" id="newEntry" name="term" class="form-control"/>
            </div>
            <input type="submit" value="Criar" class="btn btn-default btn-sm"/>
        </form>
        </div>
    else <br/>
};

declare
  %templates:wrap
  function academia:dump-list-box($node as node()*, $model as map(*)) as element(div) {
    if (academia:valid-user()) then
    <div style="margin-top: 15px">
        <form action="list.xq" method="POST" enctype='multipart/form-data'>
            <label for="file" style="margin-right: 10px;">Listar entradas a partir de ficheiro:</label>
            <div><input style="margin-bottom: 10px;" type="file" id="file" name="file"/></div>
            <input type="submit" value="Calcular" class="btn btn-default btn-sm"/>
        </form>
        </div>
    else <div></div>
};


declare 
  function academia:user-name($node as node()*, $model as map(*)) as element(span) {
    <span> { sm:id()//sm:username/text() } </span>
};

declare
  %templates:wrap
  function academia:pages-tools($node as node()*, $model as map(*)) as element(div) {
    if (academia:valid-user()) then
        <div style="margin-top: 15px">
        <div style="width: 50%; float: left">
        <form action="newPage.html" method="POST">
            <div class="form-group">
                <label for="newDoc" style="margin-right: 10px;">Nova página:</label>
                <input type="text" size="40" id="newDoc" name="page" class="form-control"/>
            </div>
            <input type="submit" value="Criar" class="btn btn-default btn-sm"/>
        </form>
        </div>
        <br style="clear: both"/>
        </div>
    else
        <div></div>
};

(: Searches for specific word, returns XML in a div :)
declare 
  %templates:wrap
  function academia:search-word($node as node()*, $model as map(*)) as element(div) {
     <div class="results">
   {
    let $username := sm:id()//sm:username/text()
    let $searchphrase := request:get-parameter("searchphrase", ())
    let $re := concat("^", functx:escape-for-regex(fn:lower-case($searchphrase)), "(:[0-9]+)?$")
    for $entry in collection("/db/academia")//entry[matches(./form/orth/text(), $re, "i")]
    let $uri := fn:base-uri($entry)
    let $delete := <form action="delete.xq" method="POST" onsubmit="return confirm('É mesmo para apagar?');">
                <input type="hidden" name="entry" value="{ $uri }"/>
                <button type="submit" class="btn btn-danger btn-xs">Remover</button></form>
    let $edit := <form action="editor.xq" method="POST" target="_blank">
                <input type="hidden" name="entry" value="{ $uri }"/>
                <button type="submit" class="btn btn-default btn-xs">Editar</button></form>
    let $link := 
            <a href="oxygen:/academia$academia{ $uri }">{ xmldb:decode-uri($uri) } </a>
    let $debug := if (academia:can-edit())
                   then  <div class="debug"> { $delete } { $edit } <span style='margin-left: 50px;'></span> { $link } </div>
                   else  ""                       
    order by $entry/form/orth[1]
    return 
        <div class="entry"> 
            { $entry }    { $debug }
        </div>
   }
    </div>
};

(: Searches for specific word as a substring. ignores same word :)
declare 
  %templates:wrap
  function academia:search-substring($node as node()*, $model as map(*)) as element(div) {
      <div class="results">
      {
            let $searchphrase := request:get-parameter("searchphrase", ())
            return if (string-length($searchphrase) > 3)
            then
                <div><h4>Termos que contêm pesquisa:</h4>
                {
                for $entry in collection("/db/academia")//entry[
                     contains(./form/orth/text() ! fn:lower-case(.), fn:lower-case($searchphrase)) and
                     not(matches(./form/orth/text() ! fn:lower-case(.),
                         concat("^", functx:escape-for-regex(fn:lower-case($searchphrase)), "(:[0-9]+)?$")))]
                order by $entry/form/orth[1]
                return 
                    <div class="entry">
                        { $entry }
                        <div class="debug"> <a href="oxygen:/academia$academia{ fn:base-uri($entry) }">{ xmldb:decode-uri(fn:base-uri($entry)) } </a></div>    
                    </div>
                }
                </div>
            else
                <div><h4>Pesquisa demasiado pequena para pesquisar termos que a contêm!</h4></div>
      }
      </div>
};

(:  Revsearch :)
declare
  %templates:wrap
  function academia:search-rev($node as node()*, $model as map(*)) as element(div) {
   <div id="results">
            {
                let $searchphrase := request:get-parameter("searchphrase", ())
                return
                for $entry in collection("/db/academia")//entry[contains(.//text() ! fn:lower-case(.), fn:lower-case($searchphrase))]
                order by $entry/form/orth[1]
                return 
                    <div class="entry">
                        { $entry }
                        <div class="debug"> <a href="oxygen:/academia$academia{ fn:base-uri($entry) }">{ xmldb:decode-uri(fn:base-uri($entry)) } </a></div>    
                    </div>
            }
     
        </div>    
};


(: Searches for  related entries :)
declare 
  %templates:wrap
  function academia:search-re($node as node()*, $model as map(*)) as element(div) {
      <div class="results">
      {
            let $searchphrase := request:get-parameter("searchphrase", ())
            return if (string-length($searchphrase) > 3)
            then
                <div>
                    <h4>Expressões que contêm pesquisa:</h4>
                    {
                    for $entry in collection("/db/academia")//re[contains(./form/orth/text() ! fn:lower-case(.) ,
                                fn:lower-case($searchphrase))]
                    order by $entry/form/orth[1]
                    return 
                        <div class="entry">
                            { $entry }
                            <div class="debug">  <a href="oxygen:/academia$academia{ fn:base-uri($entry) }">{ xmldb:decode-uri(fn:base-uri($entry)) } </a> </div>    
                        </div>
                    }
                </div>
            else
                <div><h4>Pesquisa demasiado pequena para pesquisar expressões que a contêm!</h4></div>
      }
    </div>
};


declare function academia:createPage ( $term as xs:string, $resource-name as xs:string )  as xs:string {
  let $collection-uri := "/db/academiaPages"                            
  let $contents       := 
     <h1> {$term }</h1>
  let $mime-type      := "text/xml"
  return xmldb:store($collection-uri, $resource-name, $contents, $mime-type)
};

declare function academia:createDoc ( $term as xs:string, $resource-name as xs:string )  as xs:string {
  let $collection-uri := "/db/academia"                            
  let $contents       := 
     <entry xmlns:m="http://www.w3.org/1998/Math/MathML" 
            xmlns:vc="http://www.w3.org/2007/XMLSchema-versioning"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:noNamespaceSchemaLocation="http://per-fide.di.uminho.pt:8080/exist/rest/db/schemas/academia-new.xsd">
        <meta status="new"/>
        <form><orth>{$term}</orth><pron> FIXME </pron></form>
        <group>
            <gramGrp>n. m.</gramGrp>
            <sense>
              <def>Definição de {$term}</def>
            </sense>
        </group>
     </entry>
  let $mime-type      := "text/xml"
  return xmldb:store($collection-uri, $resource-name, $contents, $mime-type)
};

declare function academia:checkDoc ( $arg as xs:string )  as xs:boolean {
    if (xmldb:document($arg)) then
        true()
    else 
        false()
}; 

declare function academia:term-available ( $myTerm as xs:string ) as xs:boolean {
let $term := replace( $myTerm , ":", "_")
let $lcterm := lower-case($term)
let $ucterm := upper-case($lcterm)
let $ucfterm := functx:capitalize-first($lcterm)
let $sequence := ($lcterm, $ucterm, $ucfterm, concat($lcterm,"_1"), concat($ucterm,"_1"), concat($ucfterm,"_1")) ! concat("/db/academia/", . , ".xml")
let $f := academia:checkDoc#1
let $existing := filter( $sequence, $f)
return 
    if (fn:count($existing) = 0) then true() else false()
};

declare function academia:page-available ( $myTerm as xs:string ) as xs:boolean {
  if (xmldb:document(concat("/db/academiaPages/", $myTerm))) then false() else true()
};


(: -------------- REPORTS --------------- :)

declare function academia:report( $type as xs:string ) as node()+ {
    processing-instruction xml-stylesheet {'type="text/css" href="http://per-fide.di.uminho.pt:8080/exist/apps/academia/resources/css/style.css"'}
,
    let $title := 
      if ($type = "new") 
       then "Novas Entradas"
      else if ($type = "edited")
       then "Entradas Editadas" 
       else "Entradas Revistas"
    let $entries := 
      if ($type = "new") 
       then collection("/db/academia")/entry[.//meta[@status="novo" or @status="new"]]
      else if ($type = "edited") 
       then collection("/db/academia")/entry[.//meta[@status="edited" or @status="editado"]]
       else collection("/db/academia")/entry[.//meta[@status="revised" or @status="revisto"]] 
    return <report>
    <section title="{ $title }">
{ for $x in $entries
  order by $x/form/orth[1]
  return $x
}
    </section>
</report>
};

