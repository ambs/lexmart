




var currentMode = "laic";
   
function toggleMode() {
    currentMode = currentMode == "laic" ? "nerd" : "laic";
    Xonomy.setMode(currentMode);
}

function currentDate() {
    var date = new Date();
    
    var mon = date.getMonth() + 1;
    mon = mon < 10 ? "0" + mon : mon;
    
    var day = date.getDate();
    day = day < 10 ? "0" + day : day;
    
    return date.getFullYear() + "-" + mon + "-" + day;
}


function loadList(x, varname) {
    var url = "/exist/resources/xml/" + x; 
    $.ajax( { url: url, dataType: "xml" } )
     .done(
        function(xml) {
            var $data = $( xml );
            var items = $data.find("item");
            var list = [];
            $.each($(items), function(i, el) {
                list.push($(el).find("key").text());
            });
            window[varname] = list;
        }
    );
}

var gramGrpList; loadList("gramgroups.xml", "gramGrpList");
var usgDomList;  loadList("sdomains.xml", "usgDomList");
var usgGeoList;  loadList("geographics.xml", "usgGeoList");
var usgRegList;  loadList("sregisters.xml", "usgRegList");
    
Xonomy.askGramGrp      = function(defaultString) { return makeMenu(defaultString, gramGrpList)}
Xonomy.askUsgDom       = function(defaultString) { return makeMenu(defaultString, usgDomList )}
Xonomy.askUsgGeo       = function(defaultString) { return makeMenu(defaultString, usgGeoList )}
Xonomy.askUsgRegister  = function(defaultString) { return makeMenu(defaultString, usgRegList )}
    

function makeMenu(defaultString, x) {
    html ="<div class='menu'>";
	for(var i=0; i<x.length; i++) {
		var item = { value: x[i], caption: ""};
		html+="<div class='menuItem techno"+(item.value==defaultString?" current":"")+"' onclick='Xonomy.answer(\""+Xonomy.xmlEscape(item.value)+"\")'>";
		html+="<span class='punc'>\"</span>";
		html+=Xonomy.xmlEscape(item.value);
		html+="<span class='punc'>\"</span>";
		if(item.caption!="") html+=" <span class='explainer'>"+Xonomy.xmlEscape(Xonomy.textByLang(item.caption))+"</span>";
		html+="</div>";
	}
	html+="</div>";
    return html;
}


    
var struct = {
    elements: {
        "entry": {
            menu: [
                {
                    caption: "Marcar como apenas digital",
                    action: Xonomy.newAttribute,
                    actionParameter: { name: "digital", value: "true" },
                    hideIf: function(jEl){ return jEl.hasAttribute("digital"); }
                }
             ],
            "attributes": { 
                "xsi:noNamespaceSchemaLocation" : {},
                "digital" : {menu: [ {caption: "Remover", action: Xonomy.deleteAttribute }]}
                }
        },
        "meta": { 
            oneliner: true,
            attributes: {
                "status" : {    
                    asker: Xonomy.askPicklist,
                    askerParameter: [
                        { value: 'edited', caption: "Editado" },
                        { value: 'reviewed', caption: "Revisto" }
                    ]
                }
            }
        },
        "group": {
            menu: [
                {
                    caption: "Adicionar novo grupo gramatical",
                    action: Xonomy.newElementAfter, 
                    actionParameter: "<group><gramGrp>...</gramGrp><sense><def>...</def></sense></group>"
                },
                {
                    caption: "Adicionar Combinatória Independente",
                    action: Xonomy.newElementChild,
                    actionParameter: "<re><form><orth>foo bar</orth></form><sense><def>...</def></sense></re>"
                },
                {
                    caption: "Adicionar etimologia",
                    action: Xonomy.newElementChild, 
                    actionParameter: "<etym>...</etym>",
                    hideIf: function(jEl){ return jEl.hasChildElement("etym"); }
                },
                {
                    caption: "Adicionar nota",
                    action: Xonomy.newElementChild, 
                    actionParameter: "<note>...</note>",
                    hideIf: function(jEl){ return jEl.hasChildElement("note"); }
                },
                {
                    caption: "Remover este grupo", 
                    action: Xonomy.deleteElement
                },
                {
                    caption: "Marcar como apenas digital",
                    action: Xonomy.newAttribute,
                    actionParameter: { name: "digital", value: "true" },
                    hideIf: function(jEl){ return jEl.hasAttribute("digital"); }
                }
             ],
            "attributes": { 
                "digital" : {menu: [ {caption: "Remover", action: Xonomy.deleteAttribute }]}
                }
        },
        "form": {
            hasText: false
        },
        "orth": {
            oneliner: true,
             menu: [
                {
                    caption: "Adicionar forma aAO",
                    action: Xonomy.newAttribute,
                    actionParameter: { name: "aAO", value: "..." },
                    hideIf: function(jEl){ return jEl.hasAttribute("aAO"); }
                },
                {
                    caption: "Adicionar forma feminina",
                    action: Xonomy.newAttribute,
                    actionParameter: { name: "fem", value: "a" },
                    hideIf: function(jEl){ return jEl.hasAttribute("fem"); }
                },
                {
                    caption: "Remover forma ortográfica",
                    action: Xonomy.deleteElement
                }
             ],
              attributes: {
                aAO : { asker: Xonomy.askString,
                    menu: [ {caption: "Remover", action: Xonomy.deleteAttribute }]
                   },
                   fem : { asker: Xonomy.askString,
                    menu: [ {caption: "Remover", action: Xonomy.deleteAttribute }]
                   }
            }
        },
        "pron": {
            oneliner: true, 
            menu: [
                {
                    caption: "Remover",
                    action: Xonomy.deleteElement
                }
            ]
        },
        "xr": {
            oneliner: true, 
            menu: [
                {
                    caption: "Apagar",
                    action: Xonomy.deleteElement
                },
                {
                    caption: "Apagar referência",
                    action: Xonomy.unwrap
                }
            ]
        },
        "ref": {
            oneliner: true,
            menu: [
            {
                caption: "Remover referência",
                action: Xonomy.unwrap
            }
          ]
        },
        "usgHint": {
            oneliner: true,
            asker: Xonomy.askString,
            menu: [
                {
                    caption: "Remover",
                    action: Xonomy.deleteElement
                }
            ]
        },
        "gramGrp": {
            oneliner: true,
            asker: Xonomy.askGramGrp,
            menu: [
                {
                    caption: "Remover",
                    action: Xonomy.deleteElement
                }
            ]
        },
        "usgDom": {
            oneliner: true,
            asker: Xonomy.askUsgDom,
            mustBeBefore: ['def', 'usgRegister'],
            mustBeAfter: ['usgGeo'],
            menu: [
                {
                    caption: "Remover",
                    action: Xonomy.deleteElement
                },
                {
                    caption: "Adicionar contexto",
                    action: Xonomy.newAttribute,
                    actionParameter: { name: "context", value: "contexto"},
                    hideIf: function(jEl){ return jEl.hasAttribute("context"); }
                 }
            ],
            attributes: {
                context : {
                    asker: Xonomy.askString,
                    menu: [ {caption: "Remover", action: Xonomy.deleteAttribute }]}
            }
        },
        "usgGeo": {
            oneliner: true,
            asker: Xonomy.askUsgGeo,
            mustBeBefore: ['def','usgRegister','usgDom'],
            
            menu: [
                {
                    caption: "Remover",
                    action: Xonomy.deleteElement
                }
            ]
        },
        "note": {
            oneliner: true,
            mustBeAfter: ['sense','etym'],
            menu: [
                {
                    caption: "Remover",
                    action: Xonomy.deleteElement
                }
            ]
        },
        "usgRegister": {
            oneliner: true,
            asker: Xonomy.askUsgRegister,
            mustBeBefore: ['def'],
            mustBeAfter: ['usgDom','usgGeo'],
            menu: [
                {
                    caption: "Remover",
                    action: Xonomy.deleteElement
                }
            ]
        },
        "mentioned": {
          oneliner: true,
          menu: [
            {
                caption: "Remover menção",
                action: Xonomy.unwrap
            }
          ]
        },
        "i": {
          oneliner: true,
          menu: [
            {
                caption: "Remover itálico",
                action: Xonomy.unwrap
            }
          ]
        },
        "pag": {
          oneliner: true,
          menu: [
            {
                caption: "Remover página",
                action: Xonomy.unwrap
            }
          ]
        },
        "b": {
          oneliner: true,
          menu: [
            {
                caption: "Remover negrito",
                action: Xonomy.unwrap
            }
          ]
        },
        "title": {
            oneliner: true
        },
        "etym": {
            mustBeAfter: ['sense','note'],
            oneliner: true,
            hasText: true,
            menu: [{
                    caption: "Marcar como revisto",
                    action: Xonomy.newAttribute,
                    actionParameter: { name: "revisto", value: currentDate() },
                    hideIf: function(jEl){ return jEl.hasAttribute("revisto") || jEl.hasAttribute("rever");; }
                },
                {
                    caption: "Marcar para rever",
                    action: Xonomy.newAttribute,
                    actionParameter: { name: "rever", value: currentDate() },
                    hideIf: function(jEl){ return jEl.hasAttribute("revisto") || jEl.hasAttribute("rever"); }
                }],
             inlineMenu: [
                {
                    caption: "Termo noutra língua",
                    action: Xonomy.wrap,
                    actionParameter: {template: "<mentioned>$</mentioned>", placeholder: "$"}
                },
                 {
                    caption: "Referência",
                    action: Xonomy.wrap,
                    actionParameter: {template: "<xr><ref>$</ref></xr>", placeholder: "$"}
                }
            ],
            attributes: {
                revisto : { menu: [ {caption: "Remover", action: Xonomy.deleteAttribute }]},
                rever : { menu: [ {caption: "Remover", action: Xonomy.deleteAttribute }]}
            }
        },
        "bibl": {
            hasText: true,
            inlineMenu: [
                {
                    caption: "Título",
                    action: Xonomy.wrap,
                    actionParameter: {template: "<title>$</title>", placeholder: "$"}
                },
                {
                    caption: "Página",
                    action: Xonomy.wrap,
                    actionParameter: {template: "<pag>$</pag>", placeholder: "$"}
                }
            ]
        },
        "syn": {
            hasText: true,
            oneliner: true,
            mustBeAfter: ['def'],
            asker: Xonomy.askString,
            menu: [
                {
                    caption: "Remover",
                    action: Xonomy.deleteElement
                }
            ]
        },
        "ant": {
            hasText: true,
            oneliner: true,
            mustBeAfter: ['def','syn'],
            asker: Xonomy.askString,
            menu: [
                {
                    caption: "Remover",
                    action: Xonomy.deleteElement
                }
            ]
        },
        "quote": {
            mustBeAfter: ['def','syn','ant'],
            menu: [
                {
                    caption: "Remover",
                    action: Xonomy.deleteElement,
                    hideIf: function(jEl){ return !jEl.hasAttribute("type"); }
                }
            ]
        },
        "cit": {
            mustBeAfter: ['def','syn','ant','quote'],
            menu: [
                {
                    caption: "Remover",
                    action: Xonomy.deleteElement 
                }
            ]
        },
        "re": {
            mustBeAfter: ['def','syn','ant','quote','cit','sense'],
            mustBeBefore: ['etym'],
            menu: [
                {
                    caption: "Remover",
                    action: Xonomy.deleteElement
                }
            ]
        },
        "def": {
            hasText:  true,
            inlineMenu: [
                {
                    caption: "Itálico",
                    action: Xonomy.wrap,
                    actionParameter: {template: "<i>$</i>", placeholder: "$"}
                },
                {
                    caption: "Negrito",
                    action: Xonomy.wrap,
                    actionParameter: {template: "<b>$</b>", placeholder: "$"}
                }
            ],
            menu: [
                {
                    caption: "Marcar como revisto",
                    action: Xonomy.newAttribute,
                    actionParameter: { name: "revisto", value: currentDate() },
                    hideIf: function(jEl){ return jEl.hasAttribute("revisto"); }
                },
                {
                    caption: "Adicionar domínio",
                    action: Xonomy.newElementBefore, 
                    actionParameter: "<usgDom>...</usgDom>"
                },
                {
                    caption: "Adicionar registo",
                    action: Xonomy.newElementBefore, 
                    actionParameter: "<usgRegister>...</usgRegister>"
                },
                {
                    caption: "Adicionar informação goegráfica",
                    action: Xonomy.newElementBefore, 
                    actionParameter: "<usgGeo>...</usgGeo>"
                },
                {
                    caption: "Adicionar hint",
                    action: Xonomy.newElementBefore, 
                    actionParameter: "<usgHint>...</usgHint>"
                },
                {
                    caption: "Adicionar sinónimo",
                    action: Xonomy.newElementAfter, 
                    actionParameter: "<syn>...</syn>"
                },
                {
                    caption: "Adicionar antónimo",
                    action: Xonomy.newElementAfter, 
                    actionParameter: "<ant>...</ant>"
                },
                {
                    caption: "Adicionar exemplo",
                    action: Xonomy.newElementAfter, 
                    actionParameter: "<quote type=\"example\">...</quote>"
                },
                {
                    caption: "Adicionar abonação",
                    action: Xonomy.newElementAfter, 
                    actionParameter: "<cit><quote>...</quote><bibl>AUTOR, <title>Título</title>, data ou <pag>página</pag></bibl></cit>"
                },
                {
                    caption: "Adicionar combinatória",
                    action: Xonomy.newElementAfter, 
                    actionParameter: "<re><form><orth>foo bar</orth></form><sense><def>...</def></sense></re>"
                },
                {
                    caption: "Adicionar referência a combinatória",
                    action: Xonomy.newElementAfter, 
                    actionParameter: "<re><xr><ref>termo</ref> composto</xr></re>"
                }
            ],
             attributes: {
                revisto : { menu: [ {caption: "Remover", action: Xonomy.deleteAttribute }]}
            }
        },
        "sense": {
            menu: [
                {
                    caption: "Adicionar número de aceção",
                    action: Xonomy.newAttribute,
                    actionParameter: { name: "n", value: "1"},
                    hideIf: function(jEl){ return jEl.hasAttribute("n"); }
                },
                {
                    caption: "Marcar como revisto",
                    action: Xonomy.newAttribute,
                    actionParameter: { name: "revisto", value: currentDate() },
                    hideIf: function(jEl){ return jEl.hasAttribute("revisto"); }
                },
                {
                    caption: "Marcar como novo",
                    action: Xonomy.newAttribute,
                    actionParameter: { name: "novo", value: currentDate() },
                    hideIf: function(jEl){ return jEl.hasAttribute("novo"); }
                },
                {
                    caption: "Adicionar nova aceção",
                    action: Xonomy.newElementAfter,
                    actionParameter: "<sense><def>definição...</def></sense>"
                },
                {
                    caption: "Remover aceção",
                    action: Xonomy.deleteElement
                },
                {
                    caption: "Marcar como apenas digital",
                    action: Xonomy.newAttribute,
                    actionParameter: { name: "digital", value: "true" },
                    hideIf: function(jEl){ return jEl.hasAttribute("digital"); }
                }
             ],
            "attributes": { 
                "digital" : {menu: [ {caption: "Remover", action: Xonomy.deleteAttribute }]},
                n: { asker: Xonomy.askString,
                    menu: [ {caption: "Remover", action: Xonomy.deleteAttribute }]},
                revisto : { menu: [ {caption: "Remover", action: Xonomy.deleteAttribute }]},
                novo : { menu: [ {caption: "Remover", action: Xonomy.deleteAttribute }]}
            }
        }
    }
};
    
var existUri;
function load_uri(uri) {
    existUri = uri;
    $.ajax({
        url: '/exist/rest' + uri,
        dataType: "text", cache: false}
    )
    .done(
        function(xml) {
            var editor = document.getElementById("editor");
           
            xml = xml.replace(/>[ \n\t\r]+</g, "><");
            xml = xml.replace(/<usg type="register">([^<]+)<\/usg>/g, "<usgRegister>$1</usgRegister>");
            xml = xml.replace(/<usg type="geo">([^<]+)<\/usg>/g, "<usgGeo>$1</usgGeo>");
            xml = xml.replace(/<usg([^>]*)type="dom"([^>]*)>([^<]+)<\/usg>/g, "<usgDom$1$2>$3</usgDom>");
            xml = xml.replace(/<usg type="hint">(([^<]|<\/?pron>|<\/?xr>|<\/?ref>)+)<\/usg>/g, "<usgHint>$1</usgHint>");
            
            Xonomy.render(xml, editor, struct);
            Xonomy.setMode("laic")
        }
    );
}
      
function save() {
    var xml = Xonomy.harvest();
    
    xml = xml.replace(/\s+xml:space='preserve'/g, "");
    
    xml = xml.replace(/<usgRegister>/g, "<usg type=\"register\">");
    xml = xml.replace(/<usgGeo>/g, "<usg type=\"geo\">");
    xml = xml.replace(/<usgDom/g, "<usg type=\"dom\""); // this way we keep the context, if it is there.
    xml = xml.replace(/<usgHint>/g, "<usg type=\"hint\">");
    xml = xml.replace(/<\/usgRegister>/g, "</usg>");
    xml = xml.replace(/<\/usgGeo>/g, "</usg>");
    xml = xml.replace(/<\/usgDom>/g, "</usg>");
    xml = xml.replace(/<\/usgHint>/g, "</usg>");
    
    
  //  console.log(xml);
    var url = "/exist/apps/academia/store.xq?uri=" + existUri;
    $.ajax({
        type: "POST",
        url: url,
        data: xml,
        cache: false,
        contentType: "text/xml",
        processData: false
        }).success(function(result) {
            if (result) {
                alert("Guardado!");
            } else {
                alert("Erro ao guardar.");
            }
        });
}














