
var current_css = "display";
        
$(function() {

    $.ajax( {
        url: "/exist/apps/academia/listPages.xq"
    }).done( function(data) {
        $.each(data.page, function(i, el) {
            var uri = "/exist/apps/academia/showPage.xq?page=" + el.uri;
            $("#pagesMenu").append("<li><a href=\""+uri+"\">" + el.title + "</a></li>");
        });
    });
            
     $("#css-switch").click(function() {
         if (current_css == "display") {
            $("#css-format").attr({href : "/exist/apps/academia/resources/css/condensed.css"});
            current_css = "condensed";
         } else {
             $("#css-format").attr({href : "/exist/apps/academia/resources/css/style.css"});
            current_css = "display";
         }
     });
                    
            
            $("etym").each( function () {
                var contents = $(this).html();
                $(this).html(contents.trim());
            });
            
            if ($("#generic").length) {
                $('[name="searchType"]').click( function() {
                    var type = $(this).attr("value");
                    switch(type) {
                        case "dom":  load("sdomains.xml","domain.xq","domain"); break;
                        case "reg":  load("sregisters.xml","register.xq","register"); break;
                        case "geo":  load("geographics.xml","geo.xq","geo"); break;
                        case "etym": load("etymologic.xml", "etym.xq", "etym"); break;
                        case "gram": load("gramgroups.xml","gramgroup.xq","gramgroup"); break;
                        case "hint": load("hints.xml","hint.xq","hint"); break;
                    }
                });
            }
        });
        
        function checkLength(elem) {
            var formId = elem.parent('form').attr('id');
            var query = $("#" + formId + ' input[name="searchphrase"]').val();
            if (query.length < 1) {
                alert("Por favor introduza uma expressão de pesquisa")
            }
            else {
                $("#" + formId).submit();
            }
        }
        
        
       function load(xmlFile, action, dropDown) {
          $.ajax(
              {
                  url: "resources/xml/" + xmlFile,
                  context: $("#generic"),
                  cache: false,
                  dataType: "xml"
                  
              })
           .done(function(data) { 
               var $data = $( data );
               var list = $data.find("item");
               var drop = $(this);
               drop.empty();
                $.each(list, function(i, el) {
                        var elt = el.firstElementChild.textContent;
                        drop.append('<option value="' + elt + '">' + elt +"</option>")
                });
               $("#genForm").attr("action", action);
               drop.attr("name", dropDown);
           });
       }

function htmldecode(s){
    window.HTML_ESC_MAP = {
    "nbsp":" ","iexcl":"¡","cent":"¢","pound":"£","curren":"¤","yen":"¥","brvbar":"¦","sect":"§","uml":"¨","copy":"©","ordf":"ª","laquo":"«","not":"¬","reg":"®","macr":"¯","deg":"°","plusmn":"±","sup2":"²","sup3":"³","acute":"´","micro":"µ","para":"¶","middot":"·","cedil":"¸","sup1":"¹","ordm":"º","raquo":"»","frac14":"¼","frac12":"½","frac34":"¾","iquest":"¿","Agrave":"À","Aacute":"Á","Acirc":"Â","Atilde":"Ã","Auml":"Ä","Aring":"Å","AElig":"Æ","Ccedil":"Ç","Egrave":"È","Eacute":"É","Ecirc":"Ê","Euml":"Ë","Igrave":"Ì","Iacute":"Í","Icirc":"Î","Iuml":"Ï","ETH":"Ð","Ntilde":"Ñ","Ograve":"Ò","Oacute":"Ó","Ocirc":"Ô","Otilde":"Õ","Ouml":"Ö","times":"×","Oslash":"Ø","Ugrave":"Ù","Uacute":"Ú","Ucirc":"Û","Uuml":"Ü","Yacute":"Ý","THORN":"Þ","szlig":"ß","agrave":"à","aacute":"á","acirc":"â","atilde":"ã","auml":"ä","aring":"å","aelig":"æ","ccedil":"ç","egrave":"è","eacute":"é","ecirc":"ê","euml":"ë","igrave":"ì","iacute":"í","icirc":"î","iuml":"ï","eth":"ð","ntilde":"ñ","ograve":"ò","oacute":"ó","ocirc":"ô","otilde":"õ","ouml":"ö","divide":"÷","oslash":"ø","ugrave":"ù","uacute":"ú","ucirc":"û","uuml":"ü","yacute":"ý","thorn":"þ","yuml":"ÿ","fnof":"ƒ","Alpha":"Α","Beta":"Β","Gamma":"Γ","Delta":"Δ","Epsilon":"Ε","Zeta":"Ζ","Eta":"Η","Theta":"Θ","Iota":"Ι","Kappa":"Κ","Lambda":"Λ","Mu":"Μ","Nu":"Ν","Xi":"Ξ","Omicron":"Ο","Pi":"Π","Rho":"Ρ","Sigma":"Σ","Tau":"Τ","Upsilon":"Υ","Phi":"Φ","Chi":"Χ","Psi":"Ψ","Omega":"Ω","alpha":"α","beta":"β","gamma":"γ","delta":"δ","epsilon":"ε","zeta":"ζ","eta":"η","theta":"θ","iota":"ι","kappa":"κ","lambda":"λ","mu":"μ","nu":"ν","xi":"ξ","omicron":"ο","pi":"π","rho":"ρ","sigmaf":"ς","sigma":"σ","tau":"τ","upsilon":"υ","phi":"φ","chi":"χ","psi":"ψ","omega":"ω","thetasym":"ϑ","upsih":"ϒ","piv":"ϖ","bull":"•","hellip":"…","prime":"′","Prime":"″","oline":"‾","frasl":"⁄","weierp":"℘","image":"ℑ","real":"ℜ","trade":"™","alefsym":"ℵ","larr":"←","uarr":"↑","rarr":"→","darr":"↓","harr":"↔","crarr":"↵","lArr":"⇐","uArr":"⇑","rArr":"⇒","dArr":"⇓","hArr":"⇔","forall":"∀","part":"∂","exist":"∃","empty":"∅","nabla":"∇","isin":"∈","notin":"∉","ni":"∋","prod":"∏","sum":"∑","minus":"−","lowast":"∗","radic":"√","prop":"∝","infin":"∞","ang":"∠","and":"∧","or":"∨","cap":"∩","cup":"∪","int":"∫","there4":"∴","sim":"∼","cong":"≅","asymp":"≈","ne":"≠","equiv":"≡","le":"≤","ge":"≥","sub":"⊂","sup":"⊃","nsub":"⊄","sube":"⊆","supe":"⊇","oplus":"⊕","otimes":"⊗","perp":"⊥","sdot":"⋅","lceil":"⌈","rceil":"⌉","lfloor":"⌊","rfloor":"⌋","lang":"〈","rang":"〉","loz":"◊","spades":"♠","clubs":"♣","hearts":"♥","diams":"♦","\"":"quot","amp":"&","lt":"<","gt":">","OElig":"Œ","oelig":"œ","Scaron":"Š","scaron":"š","Yuml":"Ÿ","circ":"ˆ","tilde":"˜","ndash":"–","mdash":"—","lsquo":"‘","rsquo":"’","sbquo":"‚","ldquo":"“","rdquo":"”","bdquo":"„","dagger":"†","Dagger":"‡","permil":"‰","lsaquo":"‹","rsaquo":"›","euro":"€"};
    if(!window.HTML_ESC_MAP_EXP)
        window.HTML_ESC_MAP_EXP = new RegExp("&("+Object.keys(HTML_ESC_MAP).join("|")+");","g");
    return s?s.replace(window.HTML_ESC_MAP_EXP,function(x){
        return HTML_ESC_MAP[x.substring(1,x.length-1)]||x;
    }):s;
}

    
                