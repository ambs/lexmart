xquery version "3.0";
import module namespace scheduler="http://exist-db.org/xquery/scheduler";

                        
                            
(:   
scheduler:schedule-xquery-cron-job("/apps/academia/jobs/domains.xq", "0 0 0 ? * * *", "Update Domains Stats per Entry/Group")
scheduler:schedule-xquery-cron-job("/apps/academia/jobs/sdomains.xq", "0 5 0 ? * * *", "Update Domains Stats per Sense") 

scheduler:schedule-xquery-cron-job("/apps/academia/jobs/registers.xq", "3 0 0 ? * * *", "Update Registers Stats per Entry/Group")
scheduler:schedule-xquery-cron-job("/apps/academia/jobs/sregisters.xq", "3 5 0 ? * * *", "Update Registers Stats per Sense")

scheduler:schedule-xquery-cron-job("/apps/academia/jobs/geographics.xq", "3 10 0 ? * * *", "Update Geographic Stats per Entry/Group")
scheduler:schedule-xquery-cron-job("/apps/academia/jobs/sgeographics.xq", "3 15 0 ? * * *", "Update Geographic Stats per Sense")

scheduler:schedule-xquery-cron-job("/apps/academia/jobs/hints.xq", "3 20 0 ? * * *", "Update Hints Stats per Entry/Group")
scheduler:schedule-xquery-cron-job("/apps/academia/jobs/shints.xq", "3 25 0 ? * * *", "Update Hints Stats per Sense")

scheduler:schedule-xquery-cron-job("/apps/academia/jobs/gramgroups.xq", "3 30 0 ? * * *", "Update GramGroups Stats per Entry/Group")
scheduler:schedule-xquery-cron-job("/apps/academia/jobs/sgramgroups.xq", "3 35 0 ? * * *", "Update GramGroups Stats per Sense")

scheduler:schedule-xquery-cron-job("/apps/academia/jobs/etymologic.xq", "3 40 0 ? * * *", "Update Etym Stats per Entry/Group")
scheduler:schedule-xquery-cron-job("/apps/academia/jobs/setymologic.xq", "3 45 0 ? * * *", "Update Etym Stats per Sense")


:)



(: 
 scheduler:get-scheduled-jobs()
 
 scheduler:delete-scheduled-job("Update Geographic Stats per Sense") 
 :)
 