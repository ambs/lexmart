xquery version "3.0";

declare namespace validate="http://exist-db.org/xquery/validation";

declare function local:foo( $report as node()+ ) as node() {
    document {
        processing-instruction xml-stylesheet {
            'type="text/css" href="http://per-fide.di.uminho.pt:8080/exist/apps/academia/resources/css/reports.css"'
        },
        $report }
};


let $report := <all>
<reports schema="new">
{
for $doc in collection("/db/academia")
let $file    := fn:base-uri($doc)
let $fileEnc := xmldb:decode-uri($file)
let $result  := validate:jaxp-report(doc($file),true())
return if ($result//status/text() = "valid")
   then () 
   else <file><uri> {$fileEnc} </uri> {$result}</file>
}
</reports>
</all>
return xmldb:store("/db/apps/academia/resources/xml", "validation.xml", local:foo($report))

