xquery version "3.0";

import module namespace functx   = "http://www.functx.com" at "/db/system/repo/functx-1.0/functx/functx.xql";
import module namespace academia = "http://per-fide.di.uminho.pt/academia" at "/db/apps/academia/modules/functions.xql";

import module namespace xmldb    = "http://exist-db.org/xquery/xmldb"; 

declare function local:remove-empty($in as xs:string*) as xs:string* {
    $in[string-length(.) > 1]
};

let $login := xmldb:login("/db", "admin", "admin.entrada")
let $remove := if (academia:checkDoc("/db/apps/academia/resources/xml/sregisters.xml")) then
       xmldb:remove("/db/apps/academia/resources/xml", "sregisters.xml") else true()
let $col := collection("/db/academia")
let $regs := $col//sense/usg[@type="register"]
let $regxml := <collection> {
   for $value in $regs/text()
   group by $key := functx:trim(replace($value, '\s+', ' '))
   order by $key ascending collation "?lang=pt-PT"
   return <item><key> {$key} </key><value> {count($value)} </value></item>
 } </collection>
return xmldb:store("/db/apps/academia/resources/xml", "sregisters.xml", $regxml)