xquery version "3.0";

declare namespace validate="http://exist-db.org/xquery/validation";

declare function local:foo( $report as node()+ ) as node() {
    document {
        processing-instruction xml-stylesheet {
            'type="text/css" href="http://per-fide.di.uminho.pt:8080/exist/apps/academia/resources/css/reports.css"'
        },
        $report }
};


let $report := <all>
<reports schema="old">
{
let $free := validation:clear-grammar-cache()
let $copy1 := xmldb:copy("/db/schemas", "/db/tests", "academia-old.xsd")
let $rename := xmldb:rename("/db/tests", "academia-old.xsd", "academia.xsd")
let $copy2 := xmldb:copy("/db/tests", "/db/schemas", "academia.xsd")
for $doc in collection("/db/academia")
let $file := fn:base-uri($doc)
let $fileEnc := xmldb:decode-uri($file)
let $result := validate:jaxp-report(doc($file),true())
return if ($result//status/text() = "valid")
   then () 
   else <file><uri> {$fileEnc} </uri> {$result}</file>
}
</reports>

<reports schema="new">
{
let $free   := validation:clear-grammar-cache()
let $copy1  := xmldb:copy("/db/schemas", "/db/tests", "academia-new.xsd")
let $rename := xmldb:rename("/db/tests", "academia-new.xsd", "academia.xsd")
let $copy2  := xmldb:copy("/db/tests", "/db/schemas", "academia.xsd")
for $doc in collection("/db/academia")
let $file    := fn:base-uri($doc)
let $fileEnc := xmldb:decode-uri($file)
let $result  := validate:jaxp-report(doc($file),true())
return if ($result//status/text() = "valid")
   then () 
   else <file><uri> {$fileEnc} </uri> {$result}</file>
}
</reports>
</all>
return xmldb:store("/db/apps/academia/resources/xml", "validation.xml", local:foo($report))

