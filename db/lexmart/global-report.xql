xquery version "3.0";

import module namespace academia="http://per-fide.di.uminho.pt/academia" at "/db/apps/academia/modules/functions.xql";
let $type := request:get-parameter("type", ())
let $report := academia:report($type)
return $report