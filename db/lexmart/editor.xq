xquery version "3.0";

import module namespace templates = "http://exist-db.org/xquery/templates";
import module namespace config    = "http://per-fide.di.uminho.pt/academia/config"    at "modules/config.xqm"    ;
import module namespace app       = "http://per-fide.di.uminho.pt/academia/templates" at "modules/app.xql"       ;
import module namespace academia  = "http://per-fide.di.uminho.pt/academia"           at "modules/functions.xql" ;
import module namespace functx    = "http://www.functx.com"    at "/db/system/repo/functx-1.0/functx/functx.xql" ;

declare namespace output = "http://www.w3.org/2010/xslt-xquery-serialization";

declare option output:method     "html5";
declare option output:media-type "text/html";

let $lookup := function($functionName as xs:string, $arity as xs:int) {
    try {
        function-lookup(xs:QName($functionName), $arity)
    } catch * {
        ()
    }
}

let $config := map {
    $templates:CONFIG_APP_ROOT       := $config:app-root,
    $templates:CONFIG_STOP_ON_ERROR  := true(),
    $templates:CONFIG_FN_RESOLVER    := $lookup,
    $templates:CONFIG_PARAM_RESOLVER := $lookup
}
let $all := map {
    $templates:CONFIGURATION := $config
}

let $uri  := request:get-parameter("entry", ())

let $text := <div>
    <link rel="stylesheet" type="text/css" href="resources/css/myxonomy.css"/>
    <script src="resources/js/xonomy.js"/>
    <script src="resources/js/editor.js"/>
    <button onClick="toggleMode();">Alterar modo</button>
    <button onClick="save();">Guardar</button>
    <div id="editor"/>
    <button onClick="toggleMode();">Alterar modo</button>
    <button onClick="save();">Guardar</button>
    <script type="text/javascript">
        $(function() {{ load_uri("{ $uri }"); }});
    </script>
</div>

return templates:surround($text, $all, "templates/page.html", "content", "","")
